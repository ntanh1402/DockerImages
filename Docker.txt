# Redis
docker run --name myredis -d -v d:/docker_volumes/redis:/data -p 6379:6379 redis redis-server --appendonly yes 
docker exec -it myredis sh

# Ubuntu
docker run --name myubuntu -it ubuntu /bin/bash
docker exec -it myubuntu /bin/bash

#DNS server
docker run --name dns -it -p 53:53/tcp -p 53:53/udp -p 10000:10000/tcp -p 443:443 -p 80:80 -p 8080:8080 -v C:/Users/ntanh/docker_volumes/dns:/data ntanh1402/dns-nginx-php:2.0
docker exec -it dns /bin/bash

docker run --name dns -d -p 53:53/tcp -p 53:53/udp -p 10000:10000/tcp -p 443:443 -p 80:80 -p 8080:8080 -v /Users/anhnguyen/Appliedmesh/docker/volumes/dns/:/data ntanh1402/dns-nginx-php

docker run --name dns1 -d -p 53:53/tcp -p 53:53/udp -p 443:443 -p 80:80 -p 8080:8080 -v C:/Users/ntanh/docker_volumes/dns1/:/data --add-host myhost:192.168.0.101 --add-host "mypointofpurchase.com":192.168.0.101 ntanh1402/dnsmasq-nginx-php

docker run --name dns1 -d -p 53:53/tcp -p 53:53/udp -p 443:443 -p 80:80 -p 8080:8080 -v C:/Users/ntanh/docker_volumes/dns1/:/data --add-host myhost:192.168.0.101 --add-host "mypointofpurchase.com":192.168.0.101 ntanh1402/dnsmasq-nginx-php

docker run --name dns -d --restart always -p 53:53/tcp -p 53:53/udp -p 443:443 -p 80:80 -p 8080:8080 -v C:/Users/ntanh/docker_volumes/dns1/:/data ntanh1402/dnsmasq-nginx-php

docker exec -it dns1 /bin/bash

#Build
docker build --no-cache -t ntanh1402/dns-nginx-php .
docker build --no-cache -t ntanh1402/dnsmasq-nginx-php:1.0 .


-v C:/Users/ntanh/docker_volumes/dns1/hosts:/etc/hosts