#!/bin/bash
set -e

NGINX_DATA_DIR=${DATA_DIR}/nginx
DNS_HOST_FILE=${DATA_DIR}/host

create_nginx_data_dir() {
  mkdir -p ${NGINX_DATA_DIR}
  chmod -R 0755 ${NGINX_DATA_DIR}
  chown -R root:root ${NGINX_DATA_DIR}

  # populate the default webmin configuration if it does not exist
  if [ ! -d ${NGINX_DATA_DIR}/etc ]; then
    mv /etc/nginx ${NGINX_DATA_DIR}/etc
  fi
  rm -rf /etc/nginx
  ln -sf ${NGINX_DATA_DIR}/etc /etc/nginx
}

copy_hosts_file() {
  if [ ! -e ${DNS_HOST_FILE} ]; then
	cp -f /etc/hosts ${DNS_HOST_FILE}
  fi
}

create_nginx_data_dir
copy_hosts_file

# default behaviour is to launch named
echo "Starting php-fpm..."
/etc/init.d/php7.0-fpm start

echo "Starting named..."
/usr/sbin/dnsmasq -h -H ${DNS_HOST_FILE}

echo "Starting nginx..."
/usr/sbin/nginx -g "daemon off;"

